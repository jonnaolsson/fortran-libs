
! ******************************************************************************
! CURFIT constructor
function curfit_ctor (k) result(res)
    integer, intent(in) :: k
    type (curfit_t) :: res

    call res%initialize (k)
end function

subroutine curfit_initialize (self, k)
    class (curfit_t), intent(in out) :: self
    integer, intent(in) :: k

    if (k < 1 .or. k > 5) then
        stop "Degree of spline not supported (1<=k<=5 required)"
    end if

    if (self%k /= k) then
        call self%reset ()
    end if

    self%k = k
end subroutine

! ******************************************************************************
! CURFIT assignment
pure subroutine curfit_assign (lhs, rhs)
    type (curfit_t), intent(out), target :: lhs
    class (curfit_t), intent(in) :: rhs

    call lhs%reset ()

    lhs%ier_fit = rhs%ier_fit

    lhs%k = rhs%k
    lhs%n = rhs%n
    lhs%ssr = rhs%ssr
    lhs%s = rhs%s

    allocate (lhs%t(lhs%n), source=rhs%t)
    allocate (lhs%c(lhs%n), source=rhs%c)

    ! if called without errors, set pointers
    if (lhs%ier_fit < 0) then
        lhs%knots => lhs%t(1:lhs%n)
        lhs%coefs => lhs%c(1:lhs%n)
    end if

end subroutine

subroutine curfit_assert_allocated (self, m)
    class (curfit_t), intent(in out) :: self
    integer, intent(in) :: m
    integer :: k

    k = self%k
    self%nest = max(m + k + 1, 2*k + 3)
    self%lwrk = m*(k + 1) + self%nest * (7 + 3 * k)

    call cond_allocate (self, self%wrk, self%lwrk)
    call cond_allocate (self, self%t, self%nest)
    call cond_allocate (self, self%c, self%nest)

    if (allocated(self%iwrk)) then
        if (size(self%iwrk) < self%nest) then
            deallocate (self%iwrk)
            allocate (self%iwrk(self%nest))
            self%ier_fit = NOT_CALLED
        end if
    else
        allocate (self%iwrk(self%nest))
        self%ier_fit = NOT_CALLED
    end if
end subroutine

subroutine cond_allocate (self, arr, n, val)
    class (curfit_t), intent(in out) :: self
    real (PREC), intent(in out), dimension(:), allocatable :: arr
    integer, intent(in) :: n
    real (PREC), intent(in), optional :: val

    if (allocated(arr)) then
        if (size(arr) >= n) return
    end if

    if (allocated(arr)) deallocate (arr)
    allocate (arr(n))
    self%ier_fit = NOT_CALLED
    if (present(val)) arr = val

end subroutine

pure subroutine curfit_reset (self)
    class (curfit_t), intent(in out) :: self

    if (allocated(self%wrk)) deallocate (self%wrk)
    if (allocated(self%w)) deallocate (self%w)
    if (allocated(self%iwrk)) deallocate (self%iwrk)
    if (allocated(self%t)) deallocate (self%t)
    if (allocated(self%c)) deallocate (self%c)

    self%knots => null()
    self%coefs => null()

    self%ier_fit = NOT_CALLED
    self%last_status = NOT_CALLED
    self%n = -1

    self%nest = -1
    self%lwrk = -1
    self%ssr = 0.0_PREC
    self%s = 0.0_PREC

end subroutine

subroutine curfit_fit (self, iopt, x, y, w, s, xb, xe)
    class (curfit_t), intent(in out), target :: self
    integer, intent(in), optional :: iopt
    real (PREC), intent(in), dimension(:) :: x, y
    real (PREC), intent(in), dimension(:), optional, target :: w
    real (PREC), intent(in), optional :: s, xe, xb

    external :: curfit

    real (PREC), dimension(:), pointer :: ptr_w
    integer :: m, liopt, k
    real (PREC) :: lxb, lxe

    ! check for conformable arrays
    self%ier_fit = INVALID_INPUT
    if (size(x) /= size(y)) return
    if (present(w)) then
        if (size(x) /= size(w)) return
    end if

    ! initialize default values
    k = self%k
    liopt = -1
    m = size(x)
    lxb = x(1)
    lxe = x(m)
    ! lower bound of recommended interval for s
    if (present(w)) self%s = m - sqrt(2*real(m, PREC))

    ! override default values if any of the optional parameters were provided
    if (present(iopt)) liopt = iopt
    if (present(xb)) lxb = xb
    if (present(xe)) lxe = xe
    if (present(s)) self%s = s

    ! optional weights
    if (present(w)) then
        ptr_w => w
    else
        call cond_allocate (self, self%w, m, 1.0_PREC)
        ptr_w => self%w
    end if

    ! allocate working arrays
    call self%assert_allocated (m)

    ! call fitpack routine
    call curfit (liopt, m, x, y, ptr_w, lxb, lxe, self%k, self%s, &
            self%nest, self%n, self%t, self%c, &
            self%ssr, self%wrk, self%lwrk, self%iwrk, self%ier_fit)

    self%knots => self%t(1:self%n)
    self%coefs => self%c(1:self%n)

    self%last_status = self%ier_fit

end subroutine

subroutine curfit_eval (self, x, ext)
    class (curfit_t), intent(in out) :: self
    real (PREC), intent(in), dimension(:) :: x
    integer, intent(in), optional :: ext

    external :: splev

    integer :: m

    ! check that spline was previously fitted to data
    self%last_status = NOT_CALLED
    if (self%ier_fit > 0) return

    m = size(x)

    call splev (self%t, self%n, self%c, self%k, x, m, ext, self%last_status)

end subroutine


! ******************************************************************************
! ATTRIBUTES

pure function curfit_status (self) result(res)
    class (curfit_t), intent(in) :: self
    integer :: res
    res = self%last_status
end function

pure function curfit_s (self) result(res)
    class (curfit_t), intent(in) :: self
    real (PREC) :: res
    res = self%s
end function

pure function curfit_ssr (self) result(res)
    class (curfit_t), intent(in) :: self
    real (PREC) :: res
    res = self%ssr
end function

!subroutine curfit95_query (m, k, lwrk, nest)
!    integer, intent(in) :: m
!    integer, intent(out), optional :: lwrk, nest, k
!
!    integer :: lk, llwrk, lnest
!
!    ! assume cubic spline by default
!    lk = 3
!    if (present(k)) lk = k
!
!    lnest = max(m + lk + 1, 2*lk + 3)
!    llwrk = lm*(lk + 1) + lnest * (7 + 3 * lk)
!
!    if (present(lwrk)) lwrk = llwrk
!    if (present(nest)) nest = lnest
!
!end subroutine
!
!subroutine curfit95_real64 (iopt, x, y, w, xb, xe, k, s, n, t, c, fp, &
!    wrk, iwrk, ier)
!
!    integer, parameter :: PREC = real64
!    integer, intent(in), optional :: iopt
!    real (PREC), intent(in), dimension(:) :: x, y
!    real (PREC), intent(in), dimension(:), optional :: w
!    real (PREC), intent(in), optional :: xb, xe, s
!    integer, intent(out) :: n
!    integer, intent(in), optional :: k
!    real (PREC), intent(in out), dimension(:) :: t, c
!    real (PREC), intent(out), optional :: fp
!    real (PREC), intent(in out), dimension(:), optional :: wrk
!    integer, intent(in out), optional :: iwrk
!    integer, intent(out), optional :: ier
!
!    real (PREC), dimension(:), pointer :: ptr_w, ptr_wrk
!    integer :: lm, lk, lier, liopt, lnest, llwrk
!    real (PREC) :: lxb, lxe, ls
!    integer, dimension(:), pointer :: ptr_iwrk
!
!    external :: curfit
!
!    ! initialize default values
!    liopt = -1
!    lk = 3
!    lfp = 0.0_PREC
!    lm = size(x)
!    lxb = x(1)
!    lxe = x(m)
!    ls = 0.0_PREC
!    ! lower bound of recommended interval for s
!    if (present(w)) ls = lm - sqrt(2*lm)
!
!    ! override default values if any of the optional parameters were provided
!    if (present(iopt)) liopt = iopt
!    if (present(xb)) lxb = xb
!    if (present(xe)) lxe = xe
!    if (present(k)) lk = k
!    if (present(s)) ls = s
!
!    ! get default working array size
!    call curfit95_query (lm, lk, llwrk, lnest)
!
!    ! optional weights
!    if (present(w)) then
!        ptr_w => w
!    else
!        allocate (ptr_w(m))
!        ptr_w = 1.0_PREC
!    end if
!
!    if (size(w) /= lm .or. size(y) /= lm) then
!        stop "Shapes of x, y, w must be equal"
!    end if
!
!    if (lk < 1 .or. lk > 5) then
!        stop "Degree of spline not supported (1<=k<=5 required)"
!    end if
!
!    if (present(wrk)) then
!        ptr_wrk => wrk
!        llwrk = size(wkr)
!    else
!        allocate (wrk(llwrk))
!    end if
!
!    if (present(iwrk)) then
!        ptr_iwrk => iwrk
!    else
!        allocate (iwrk(nest))
!    end if
!
!    if (size(iwrk) < lnest .or. size(c) < lnest .or. size(t) < lnest) then
!        stop "Arrays c, t or iwrk of insufficient size"
!    end if
!
!    call curfit (liopt, lm, x, y, ptr_w, lxb, lxe, lk, ls, lnest, n, t, c, &
!        lfp, ptr_wrk, llwrk, ptr_iwrk, lier)
!
!    ! write back return values
!    if (present(fp)) fp = lfp
!    if (present(ier)) ier = lier
!
!    ! Clean up local variables
!    if (.not. present(w)) deallocate (ptr_w)
!    if (.not. present(wrk)) deallocate (ptr_wrk)
!    if (.not. present(iwrk)) deallocate (ptr_iwrk)
!
!end subroutine
