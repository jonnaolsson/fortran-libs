module fitpack08

    use iso_fortran_env
    implicit none
    private

    integer, parameter :: PREC = real64

    integer, parameter :: NOT_CALLED = 100
    integer, parameter :: INVALID_INPUT = 10

    type :: curfit_t
        private

        integer :: k = 3
        integer :: n = -1
        real (PREC), public :: ssr = 0.0_PREC, s = 0.0_PREC

        integer :: nest = -1, lwrk = -1
        integer :: ier_fit = NOT_CALLED, last_status = NOT_CALLED
        real (PREC), dimension(:), allocatable :: wrk, t, c
        integer, dimension(:), allocatable :: iwrk
        real (PREC), dimension(:), allocatable :: w
        real (PREC), dimension(:), pointer :: knots => null(), coefs => null()

    contains
        procedure, pass :: assert_allocated => curfit_assert_allocated
        procedure, public, pass :: initialize => curfit_initialize
        procedure, public, pass :: fit => curfit_fit
        procedure, public, pass :: eval => curfit_eval
        procedure, pass :: reset => curfit_reset

        procedure, public, pass :: get_status => curfit_status
        procedure, public, pass :: get_s => curfit_s
        procedure, public, pass :: get_ssr => curfit_ssr

    end type

    interface curfit_t
        module procedure curfit_ctor
    end interface

    interface assignment (=)
        module procedure curfit_assign
    end interface

    public :: curfit_t, assignment (=)

contains

include "curfit_impl.f90"

end module
