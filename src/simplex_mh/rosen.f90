! Michael F. Hutt
! http://www.mikehutt.com
! Mar. 31, 1998
! $Id: frosen.f90,v 1.4 2007/07/10 12:45:32 mike Exp $
!
! This program will attempt to minimize Rosenbrock's function using the 
! Nelder-Mead simplex method. The program was originally developed in C. 
! To be consistent with the way arrays are handled in C, all arrays will 
! start from 0.
!
! to compile this program with g77 use:
! g77 -ffree-form -o frosen.exe frosen.f

! * Copyright (c) 1998-2004 <Michael F. Hutt>
! *
! * Permission is hereby granted, free of charge, to any person obtaining
! * a copy of this software and associated documentation files (the
! * "Software"), to deal in the Software without restriction, including
! * without limitation the rights to use, copy, modify, merge, publish,
! * distribute, sublicense, and/or sell copies of the Software, and to
! * permit persons to whom the Software is furnished to do so, subject to
! * the following conditions:
! *
! * The above copyright notice and this permission notice shall be
! * included in all copies or substantial portions of the Software.
! *
! * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
! * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
! * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
! * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
! * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
!
!
! Converted program from Fortran 77 to Fortran 90
! This program will attempt to minimize Rosenbrock's function using the 
! Nelder-Mead simplex method. The program was originally developed in C. 
! To be consistent with the way arrays are handled in C, all arrays will
! start from 0.
! compiles with ELF90
! ======================================================================
! Start of main program
program frosen

    use iso_fortran_env
    use simplex_mh_mod
    implicit none
    real :: sp(0:1) 
    integer :: d=2, iprint
    real :: e=1.0e-4, scale=1.0
    
    ! variables for double precision tests
    real (real64), parameter :: sp64(2) = [-1.2d0, 1.0d0], &
            e64 = 1.0d-4, scale64 = 1.0d0
    
    sp(0) = -1.2
    sp(1) = 1.0
    iprint = 0 ! set to 0 to get step by step print out

    CALL simplex(func, sp,d,e,scale,iprint)
    
    ! Test with double precision data
    call simplex(func64, sp64, d, e64, scale64, iprint)
    

! End of main program

CONTAINS

! ======================================================================
! This is the function to be minimized

real function func(x) result(rosen) 
  implicit none
  real, intent (in) :: x(:)
  rosen = (100*((x(2)-x(1)**2)**2)+(1.0-x(1))**2)
end function func

real (real64) function func64(x) result(rosen) 
  implicit none
  real (real64), intent (in) :: x(:)
  rosen = (100*((x(2)-x(1)**2)**2)+(1.0-x(1))**2)
end function

end program