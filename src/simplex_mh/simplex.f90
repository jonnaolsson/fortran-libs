! Michael F. Hutt
! http://www.mikehutt.com
! Mar. 31, 1998
! $Id: frosen.f90,v 1.4 2007/07/10 12:45:32 mike Exp $
!
! This program will attempt to minimize Rosenbrock's function using the 
! Nelder-Mead simplex method. The program was originally developed in C. 
! To be consistent with the way arrays are handled in C, all arrays will 
! start from 0.
!
! to compile this program with g77 use:
! g77 -ffree-form -o frosen.exe frosen.f

! * Copyright (c) 1998-2004 <Michael F. Hutt>
! *
! * Permission is hereby granted, free of charge, to any person obtaining
! * a copy of this software and associated documentation files (the
! * "Software"), to deal in the Software without restriction, including
! * without limitation the rights to use, copy, modify, merge, publish,
! * distribute, sublicense, and/or sell copies of the Software, and to
! * permit persons to whom the Software is furnished to do so, subject to
! * the following conditions:
! *
! * The above copyright notice and this permission notice shall be
! * included in all copies or substantial portions of the Software.
! *
! * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
! * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
! * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
! * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
! * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
!
!
! Converted program from Fortran 77 to Fortran 90
! This program will attempt to minimize Rosenbrock's function using the 
! Nelder-Mead simplex method. The program was originally developed in C. 
! To be consistent with the way arrays are handled in C, all arrays will
! start from 0.
! compiles with ELF90

module simplex_mh_mod

    use iso_fortran_env, only : real32, real64
    implicit none
    private
    
    interface 
        function func_real64(x) result(fx)
            import real64
            real (real64), intent(in), dimension(:) :: x
            real (real64) :: fx
        end function
        
        function func_real32(x) result(fx)
            import real32
            real (real32), intent(in), dimension(:) :: x
            real (real32) :: fx
        end function
    end interface
    
    interface simplex
        module procedure simplex_real32, simplex_real64
    end interface simplex
    
    public :: simplex

contains    

! ======================================================================
! This is the simplex routine

subroutine simplex_real32(func, start, n, EPSILON, scale, iprint)
    procedure (func_real32) :: func
    ! define precision for reals in included file    
    integer, parameter :: PREC = real32
    
    include "simplex_impl.finc"
end subroutine

subroutine simplex_real64(func, start, n, EPSILON, scale, iprint)
    procedure (func_real64) :: func
    ! define precision for reals in included file    
    integer, parameter :: PREC = real64
    
    include "simplex_impl.finc"
end subroutine

end module